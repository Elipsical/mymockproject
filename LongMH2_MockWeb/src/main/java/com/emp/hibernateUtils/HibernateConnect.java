package com.emp.hibernateUtils;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import com.emp.model.BookingOffice;
import com.emp.model.Car;
import com.emp.model.Employee;
import com.emp.model.ParkingLot;
import com.emp.model.Ticket;
import com.emp.model.Trip;


public class HibernateConnect {
	private final static SessionFactory FACTORY;

	static {
		Configuration conf = new Configuration();
		Properties props = new Properties();
		// set gía trị cho properties
		props.put(Environment.DRIVER, "oracle.jdbc.OracleDriver");
		props.put(Environment.URL, "jdbc:oracle:thin:@localhost:1521:orcl");
		props.put(Environment.USER, "Long1234");
		props.put(Environment.PASS, "Long1234");
		props.put(Environment.DIALECT, "org.hibernate.dialect.Oracle8iDialect");
		props.put(Environment.SHOW_SQL, "true");
		conf.addAnnotatedClass(Employee.class);
		conf.addAnnotatedClass(Trip.class);
		conf.addAnnotatedClass(BookingOffice.class);
		conf.addAnnotatedClass(Ticket.class);
		conf.addAnnotatedClass(Car.class);
		conf.addAnnotatedClass(ParkingLot.class);
		conf.setProperties(props);
		
		ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(conf.getProperties()).build();
		FACTORY = conf.buildSessionFactory(registry);
	}
	public static SessionFactory getFactory() {
		return FACTORY;
	}
}
