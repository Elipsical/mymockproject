package com.emp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.emp.model.Employee;
import com.emp.model.ParkingLot;
import com.emp.model.Trip;
import com.emp.repository.TripRepo;

@Controller(value = "trip")
public class TripController {

	private TripRepo triprepo = new TripRepo();
	
	@RequestMapping(value = "/trip",method = RequestMethod.GET)
	public ModelAndView tripList(Model model, @RequestParam(value = "page", required = false) Long page, 
			@RequestParam(value = "search", required = false) String search) {
		ModelAndView modelAndView = new ModelAndView("views/triplist.jsp");
		if(page != null) {
			model.addAttribute("listTrip", triprepo.findAllByPage(Integer.valueOf(String.valueOf(page))));
		}
		else {
			model.addAttribute("listTrip", triprepo.findAllByPage(1));
		}
		if(search != null) {
			model.addAttribute("listTrip", triprepo.search(search));
		}
		model.addAttribute("numPage", triprepo.numPage());
		return modelAndView;
	}
	
	
	@RequestMapping(value = "/add-trip",method = RequestMethod.GET)
	public ModelAndView addTrip(Model model) {
		ModelAndView modelAndView = new ModelAndView("views/addtrip.jsp");
		model.addAttribute("trip", new Trip());
		return modelAndView;
	}
	
	@RequestMapping(value = "/add-trip",method = RequestMethod.POST)
	public String addTrip(@ModelAttribute Trip trip) {
		triprepo.save(trip);
		return "redirect:trip";
	}
	
	@RequestMapping(value = "/update-trip",method = RequestMethod.GET)
	public ModelAndView updateParkingLotView(Model model,@RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = new ModelAndView("views/updateTrip.jsp");
		model.addAttribute("trip", triprepo.findById(id));
		return modelAndView;
	}
	
	@RequestMapping(value = "/update-trip",method = RequestMethod.POST)
	public String updateParkingLot(@ModelAttribute Trip trip) {
		triprepo.update(trip);
		return "redirect:trip";
	}
	
	@RequestMapping(value = "/delete-trip",method = RequestMethod.GET)
	public String delete(@RequestParam(value = "id", required = true) Long id) {
		try {
			triprepo.delete(id);
		} catch (Exception e) {
			return "Can't delete this trip";
		}
		return "redirect:trip";
	}
}
