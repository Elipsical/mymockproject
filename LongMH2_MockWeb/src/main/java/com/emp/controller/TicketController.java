package com.emp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.emp.model.Car;
import com.emp.model.Employee;
import com.emp.model.Ticket;
import com.emp.model.Trip;
import com.emp.repository.CarRepo;
import com.emp.repository.TicketRepo;
import com.emp.repository.TripRepo;

@Controller(value = "ticket")
public class TicketController {

	private TicketRepo ticketRepo = new TicketRepo();
	private TripRepo tripRepo = new TripRepo();
	private CarRepo carRepo = new CarRepo();
	
	@RequestMapping(value = "/ticket",method = RequestMethod.GET)
	public ModelAndView ticketList(Model model, @RequestParam(value = "page", required = false) Long page, 
			@RequestParam(value = "search", required = false) String search) {
		ModelAndView modelAndView = new ModelAndView("views/ticketlist.jsp");
		if(page != null) {
			model.addAttribute("listTicket", ticketRepo.findAll(Integer.valueOf(String.valueOf(page))));
		}
		else {
			model.addAttribute("listTicket", ticketRepo.findAll(1));
		}
		if(search != null) {
			model.addAttribute("listTicket", ticketRepo.search(search));
		}
		model.addAttribute("numPage", ticketRepo.numPage());
		return modelAndView;
	}
	
	@RequestMapping(value = "/add-ticket",method = RequestMethod.GET)
	public ModelAndView addTicket(Model model) {
		ModelAndView modelAndView = new ModelAndView("views/addticket.jsp");
		model.addAttribute("tripList", tripRepo.findAll());
		model.addAttribute("carList", carRepo.findAll());
		model.addAttribute("ticket", new Ticket());
		return modelAndView;
	}
	
	@RequestMapping(value = "/add-ticket",method = RequestMethod.POST)
	public String addTicket(@ModelAttribute Ticket ticket,HttpServletRequest request) {
		String idTrip = request.getParameter("trips");
		String license = request.getParameter("license");
		Car car = carRepo.findById(license);
		Trip trip = tripRepo.findById(Long.valueOf(idTrip));
		ticket.setCar(car);
		ticket.setTrip(trip);
		System.out.println(ticket);
		ticketRepo.save(ticket);
		return "redirect:ticket";
	}
	
	@RequestMapping(value = "/delete-ticket",method = RequestMethod.GET)
	public String deleteTicket(@RequestParam(value = "id") Long id) {
		ticketRepo.delete(id);
		return "redirect:ticket";
	}
}
