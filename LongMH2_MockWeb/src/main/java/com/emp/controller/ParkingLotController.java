package com.emp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.emp.model.ParkingLot;
import com.emp.repository.ParkingLotRepo;

@Controller(value = "parkinglot")
public class ParkingLotController {

	private ParkingLotRepo pakingRepo = new ParkingLotRepo();
	
	@RequestMapping(value = "/parkinglot",method = RequestMethod.GET)
	public ModelAndView parkingLotList(Model model, @RequestParam(value = "page", required = false) Long page, 
			@RequestParam(value = "search", required = false) String search) {
		ModelAndView modelAndView = new ModelAndView("views/parkinglotlist.jsp");
		if(page != null) {
			model.addAttribute("listParking", pakingRepo.findAll(Integer.valueOf(String.valueOf(page))));
		}
		else {
			model.addAttribute("listParking", pakingRepo.findAll(1));
		}
		if(search != null) {
			model.addAttribute("listParking", pakingRepo.search(search));
		}
		model.addAttribute("numPage", pakingRepo.numPage());
		return modelAndView;
	}
	
	@RequestMapping(value = "/add-parkinglot",method = RequestMethod.GET)
	public ModelAndView addParkingLotView(Model model) {
		ModelAndView modelAndView = new ModelAndView("views/addparkinglot.jsp");
		model.addAttribute("parkingLot", new ParkingLot());
		return modelAndView;
	}
	
	@RequestMapping(value = "/add-parkinglot",method = RequestMethod.POST)
	public String AddParkingLot(@ModelAttribute ParkingLot parkingLot) {
		pakingRepo.save(parkingLot);
		System.out.println(parkingLot);
		return "redirect:parkinglot";
	}
	
	@RequestMapping(value = "/delete-parkinglot",method = RequestMethod.GET)
	public String delete(@RequestParam(value = "id", required = true) Long id) {
		try {
			pakingRepo.delete(id);
		} catch (Exception e) {
			return "Can't delete this parking lot";
		}
		return "redirect:parkinglot";
	}
	
	@RequestMapping(value = "/update-parkinglot",method = RequestMethod.GET)
	public ModelAndView updateParkingLotView(Model model,@RequestParam(value = "id", required = true) Long id) {
		ModelAndView modelAndView = new ModelAndView("views/updateParkingLot.jsp");
		model.addAttribute("parkingLot", pakingRepo.findById(id));
		return modelAndView;
	}
	
	@RequestMapping(value = "/update-parkinglot",method = RequestMethod.POST)
	public String updateParkingLot(@ModelAttribute ParkingLot parkingLot) {
		pakingRepo.update(parkingLot);
		return "redirect:parkinglot";
	}
	
}
