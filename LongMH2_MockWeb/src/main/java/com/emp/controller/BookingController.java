package com.emp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.emp.model.BookingOffice;
import com.emp.repository.BookingOfficeRepo;
import com.emp.repository.TripRepo;

@Controller(value = "booking")
public class BookingController {
	
	private BookingOfficeRepo bookingrepo = new BookingOfficeRepo();
	private TripRepo tripRepo = new TripRepo();
	
	@RequestMapping(value = "/booking",method = RequestMethod.GET)
	public ModelAndView bookingList(Model model, @RequestParam(value = "page", required = false) Long page, 
			@RequestParam(value = "search", required = false) String search) {
		ModelAndView modelAndView = new ModelAndView("views/bookinglist.jsp");
		if(page != null) {
			model.addAttribute("listBooking", bookingrepo.findAll(Integer.valueOf(String.valueOf(page))));
		}
		else {
			model.addAttribute("listBooking", bookingrepo.findAll(1));
		}
		if(search != null) {
			model.addAttribute("listBooking", bookingrepo.search(search));
		}
		model.addAttribute("numPage", bookingrepo.numPage());
		return modelAndView;
	}

	@RequestMapping(value = "/add-booking",method = RequestMethod.GET)
	public ModelAndView AddBookingView(Model model) {
		ModelAndView modelAndView = new ModelAndView("views/addbooking.jsp");
		model.addAttribute("bookingOffice", new BookingOffice());
		model.addAttribute("trip", tripRepo.findAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/add-bookings",method = RequestMethod.POST)
	public String AddBooking(@ModelAttribute BookingOffice bookingOffice,HttpServletRequest request) {
		String idTrip = request.getParameter("trips");
		bookingOffice.setTrip(tripRepo.findById(Long.valueOf(idTrip)));
		bookingrepo.save(bookingOffice);
		return "redirect:booking";
		
	}
}
