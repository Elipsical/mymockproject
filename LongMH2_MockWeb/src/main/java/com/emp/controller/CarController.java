package com.emp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.emp.model.Car;
import com.emp.model.ParkingLot;
import com.emp.repository.CarRepo;
import com.emp.repository.ParkingLotRepo;

@Controller(value = "car")
public class CarController {

	private CarRepo carRepo = new CarRepo();
	private ParkingLotRepo parkingRepo = new ParkingLotRepo();
	
	@RequestMapping(value = "/car",method = RequestMethod.GET)
	public ModelAndView carList(Model model, @RequestParam(value = "page", required = false) Long page, 
			@RequestParam(value = "search", required = false) String search) {
		ModelAndView modelAndView = new ModelAndView("views/carlist.jsp");
		if(page != null) {
			model.addAttribute("listCar", carRepo.findAll(Integer.valueOf(String.valueOf(page))));
		}
		else {
			model.addAttribute("listCar", carRepo.findAll(1));
		}
		if(search != null) {
			model.addAttribute("listCar", carRepo.search(search));
		}
		model.addAttribute("numPage", carRepo.numPage());
		return modelAndView;
	}
	
	@RequestMapping(value = "/add-car",method = RequestMethod.GET)
	public ModelAndView addCarView(Model model) {
		ModelAndView modelAndView = new ModelAndView("views/addcar.jsp");
		model.addAttribute("car", new Car());
		model.addAttribute("listparking", parkingRepo.findAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/add-car",method = RequestMethod.POST)
	public String addCar(@ModelAttribute Car car,HttpServletRequest request) {
		String idparking = request.getParameter("parking");
		ParkingLot parkingLot = parkingRepo.findById(Long.valueOf(idparking));
		car.setParkingLot(parkingLot);
		carRepo.save(car);
//		System.out.println(car);
		return "redirect:car";
	}
	
	
	
	@RequestMapping(value = "/delete-car",method = RequestMethod.GET)
	public String deleteCar(@RequestParam(value = "id") String id) {
		carRepo.delete(id);
		return "redirect:car";
	}
	
	@RequestMapping(value = "/update-car",method = RequestMethod.GET)
	public ModelAndView updateCarView(Model model,@RequestParam(value = "id") String id) {
		ModelAndView modelAndView = new ModelAndView("views/updatecar.jsp");
		model.addAttribute("car", carRepo.findById(id));
		model.addAttribute("listparking", parkingRepo.findAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/update-car",method = RequestMethod.POST)
	public String updateCar(@ModelAttribute Car car,HttpServletRequest request) {
		String idparking = request.getParameter("parking");
		ParkingLot parkingLot = parkingRepo.findById(Long.valueOf(idparking));
		car.setParkingLot(parkingLot);
		carRepo.update(car);
		return "redirect:car";
	}
}
