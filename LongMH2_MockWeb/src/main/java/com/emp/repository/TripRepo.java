package com.emp.repository;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import com.emp.hibernateUtils.HibernateConnect;
import com.emp.model.Employee;
import com.emp.model.Trip;

public class TripRepo {
	
	public List<Trip> findAllByPage(int page){
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("From Trip");
		query.setMaxResults(Limit.VALUE);
		query.setFirstResult((page-1)*Limit.VALUE);
		@SuppressWarnings("unchecked")
		List<Trip> list = query.getResultList();
		session.close();
		return list;
	}
	
	public List<Trip> findAll(){
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("From Trip");
		@SuppressWarnings("unchecked")
		List<Trip> list = query.getResultList();
		session.close();
		return list;
	}
	
	public Trip findById(Long id) {
		Session session = HibernateConnect.getFactory().openSession();
		Trip trip = session.get(Trip.class, id);
		session.close();
		return trip;
	}
	
	public void save(Trip trip) {
		Session session = HibernateConnect.getFactory().openSession();
		session.getTransaction().begin();
		session.save(trip);
		session.getTransaction().commit();
		session.close();
	}
	
	public void update(Trip trip) {
		Session session = HibernateConnect.getFactory().openSession();
		session.getTransaction().begin();
		session.update(trip);
		session.getTransaction().commit();
		session.close();
	}

	public void delete(Long id) {
		Session session = HibernateConnect.getFactory().openSession();
		session.getTransaction().begin();
		Trip trip = session.get(Trip.class, id);
		session.delete(trip);
		session.getTransaction().commit();
		session.close();
	}
	
	public List<Trip> search(String param){
		if(param == null || param.equals("")) {
			return findAllByPage(1);
		}
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("Select t FROM Trip t "
				+ "where t.carType like ?1 or t.destination like ?2 or t.driver like ?3");
		query.setParameter(1, "%"+param+"%");
		query.setParameter(2, "%"+param+"%");
		query.setParameter(3, "%"+param+"%");
		List<Trip> list = query.getResultList();
		
		session.close();
		return list;
	}
	
	public List<Long> numPage() {
		List<Long> list = new ArrayList<Long>();
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("select count(*) From Trip");
		Long num = (Long) query.getSingleResult();
		
		session.close();
		if(num <= Limit.VALUE) {
			list.add(1L);
			return list;
		}
		else {
			Long page =  (Long) ((num % Limit.VALUE == 0) ? num / Limit.VALUE : num / Limit.VALUE+1);
			for(int i=1; i<=page; i++) {
				list.add((long) i);
			}
			return list;
		}
	}
//	public static void main(String[] args) throws ParseException {
//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//		Date parsed = format.parse("2011-02-10");
//		java.sql.Date DEPARTUREDATE = new java.sql.Date(parsed.getTime());
//		
//		
//		Session session = HibernateConnect.getFactory().openSession();
//		Query query = session.createQuery("From Trip");
//		session.getTransaction().begin();
//		
//		Trip trip = new Trip();
//		trip.setBookedTicketNumber(21);
//		trip.setCarType("o to");
//		trip.setDepartureDate(DEPARTUREDATE);
//		trip.setDepartureTime(new Timestamp(System.currentTimeMillis()));
//		trip.setDestination("ko");
//		trip.setDriver("long");
//		trip.setMaximumOnlineTicketNumber(21);
//		session.save(trip);
//		List<Trip> list = query.getResultList();
//		list.forEach(p -> {
//			System.out.println(p);
//		});
//		session.getTransaction().commit();
//		session.close();
//		
//	}
}
