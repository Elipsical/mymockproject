package com.emp.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import com.emp.hibernateUtils.HibernateConnect;
import com.emp.model.BookingOffice;
import com.emp.model.Employee;

public class BookingOfficeRepo {

	public void save(BookingOffice booking) {
		Session session = HibernateConnect.getFactory().openSession();
		session.getTransaction().begin();
		session.save(booking);
		session.getTransaction().commit();
		session.close();
	}
	
	public List<BookingOffice> findAll(int page){
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("From BookingOffice");
		query.setMaxResults(Limit.VALUE);
		query.setFirstResult((page-1)*Limit.VALUE);
		@SuppressWarnings("unchecked")
		List<BookingOffice> list = query.getResultList();
		session.close();
		return list;
	}
	
	public List<Long> numPage() {
		List<Long> list = new ArrayList<Long>();
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("select count(*) From BookingOffice");
		Long num = (Long) query.getSingleResult();
		
		session.close();
		if(num <= Limit.VALUE) {
			list.add(1L);
			return list;
		}
		else {
			Long page =  (Long) ((num % Limit.VALUE == 0) ? num / Limit.VALUE : num / Limit.VALUE+1);
			for(int i=1; i<=page; i++) {
				list.add((long) i);
			}
			return list;
		}
	}
	
	public List<BookingOffice> search(String param){
		if(param == null || param.equals("")) {
			return findAll(1);
		}
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("Select b FROM BookingOffice b "
				+ "where b.officeName like ?1 or b.officePhone like ?2 or b.officePlace like ?3");
		query.setParameter(1, "%"+param+"%");
		query.setParameter(2, "%"+param+"%");
		query.setParameter(3, "%"+param+"%");
		List<BookingOffice> list = query.getResultList();
		
		session.close();
		return list;
	}
	
}
