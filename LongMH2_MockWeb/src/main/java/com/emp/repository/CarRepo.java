package com.emp.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import com.emp.hibernateUtils.HibernateConnect;
import com.emp.model.BookingOffice;
import com.emp.model.Car;
import com.emp.model.Employee;

public class CarRepo {

	public List<Car> findAll(int page){
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("From Car");
		query.setMaxResults(Limit.VALUE);
		query.setFirstResult((page-1)*Limit.VALUE);
		@SuppressWarnings("unchecked")
		List<Car> list = query.getResultList();
		session.close();
		return list;
	}
	
	public List<Car> findAll(){
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("From Car");
		@SuppressWarnings("unchecked")
		List<Car> list = query.getResultList();
		session.close();
		return list;
	}
	
	
	public List<Car> search(String param){
		if(param == null || param.equals("")) {
			return findAll(1);
		}
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("Select c FROM Car c "
				+ "where c.licensePlate like ?1 or c.carColor like ?2 or c.carType like ?3 or c.company like ?4");
		query.setParameter(1, "%"+param+"%");
		query.setParameter(2, "%"+param+"%");
		query.setParameter(3, "%"+param+"%");
		query.setParameter(4, "%"+param+"%");
		List<Car> list = query.getResultList();
		
		session.close();
		return list;
	}
	
	public Car findById(String id) {
		Session session = HibernateConnect.getFactory().openSession();
		Car c = session.get(Car.class, id);
		session.close();
		return c;
	}
	
	public List<Long> numPage() {
		List<Long> list = new ArrayList<Long>();
		Session session = HibernateConnect.getFactory().openSession();
		Query query = session.createQuery("select count(*) From Car");
		Long num = (Long) query.getSingleResult();
		
		session.close();
		if(num <= Limit.VALUE) {
			list.add(1L);
			return list;
		}
		else {
			Long page =  (Long) ((num % Limit.VALUE == 0) ? num / Limit.VALUE : num / Limit.VALUE+1);
			for(int i=1; i<=page; i++) {
				list.add((long) i);
			}
			return list;
		}
	}
	
	public void save(Car car) {
		Session session = HibernateConnect.getFactory().openSession();
		session.getTransaction().begin();
		session.save(car);
		session.getTransaction().commit();
		session.close();
	}
	
	
	public void delete(String id) {
		Session session = HibernateConnect.getFactory().openSession();
		session.getTransaction().begin();
		Car car = session.get(Car.class, id);
		session.delete(car);
		session.getTransaction().commit();
		session.close();
	}
	
	public void update(Car car) {
		Session session = HibernateConnect.getFactory().openSession();
		session.getTransaction().begin();
		session.update(car);
		session.getTransaction().commit();
		session.close();
	}
}
