	<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Employee list</title>
<link href="<c:url value='template/style.css'/>" rel="stylesheet"
	type="text/css" />
<meta name="robots" content="all,follow">
<!-- Bootstrap CSS-->
<link rel="stylesheet"
	href="<c:url value='template/vendor/bootstrap/css/bootstrap.min.css'/>">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="<c:url value='template/vendor/font-awesome/css/font-awesome.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='template/vendor/owl.carousel/assets/owl.carousel.css'/>">
<link rel="stylesheet"
	href="<c:url value='template/vendor/owl.carousel/assets/owl.theme.default.css'/>">
<style type="text/css">
body {
	background-color: #f1f1f1;
}

.emp-mn {
	float: left;
	margin-top: 10px;
}

.menu-table {
	float: right;
}

.menu-table button {
	margin-left: 10px;
}

.menu-table input {
	border: 0.5px solid #dee3e0;
}

.tables {
	margin-top: 20px;
}

.pagination {
	display: inline-block;
}

.pagination a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
}

.div-search {
	background-color: #f1f1f1;
	width: 30px;
	border: 0.5px solid #dee3e0;
}
</style>
</head>
<body>
	<!-- title -->
	<div class="col-md-12">
		<div class="row menu">
			<div class="col-sm-8">
				<p class="cms-title">
					<i class="fa fa-users"></i> Employee
				</p>
			</div>
			<div class="col-sm-2">
				<a href="#">Welcome kasjd</a>
			</div>
			<div class="col-sm-2">
				<a href="#"><i class="fa fa-sign-out"></i>Logout</a>
			</div>
		</div>
	</div>

	<!-- thanh ngang -->
	<div class="col-md-12">
		<hr>
	</div>

	<div class="col-md-12 contents">
		<div class="row">

			<!-- bên trái -->
			<div class="col-md-3 content-left">
				<a href="#"><i class="fa fa-dashboard icon-left"></i> Dashboard</a>
				<hr>
				<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-bar-chart"></i> Employee Manager</a>
					<div class="dropdown-content">
						<a href="#"><i class="fa fa-list"></i> Employee List</a> <a
							href="add-employee"><i class="fa fa-plus"></i> Add Employee</a>
					</div>
				</div>
			</div>

			<!-- bên phải -->
			<div class="col-md-9 content-right">
				<h2 class="editTitle">Employee list</h2>
				<hr>
				<div class="col-md-12">
					<div class="row">
						<div class="col-sm-4"></div>
						<div class="col-sm-8">
						<form action="employee", method="get" id="search">
							<div class="row menu-table">
								<div class="div-search">
									<p>
										<i class="fa fa-search"></i>
									</p>
								</div>
								
								<input type="text" name="search" placeholder="user search"
									class="">
								
								<button style="" class="btn-filter">
									<i class="fa fa-filter"></i>Filter By
								</button>
								<select>
									<option>Name</option>
								</select>
								<button class="btn-primary" form="search">Search</button>
							</div>
							</form>
						</div>
						<div class="col-sm-12 tables">
							<table class="table table-hover">
								<thead>
									<tr>
										<th scope="col">Id</th>
										<th scope="col">Name</th>
										<th scope="col">Date Of Birth</th>
										<th scope="col">Address</th>
										<th scope="col">Phone number</th>
										<th scope="col">Department</th>
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${listEmployee}" var="list">
									<tr>
										<th scope="row">${list.id}</th>
										<td>${list.employeeName}</td>
										<td>${list.employeeBirthDate}</td>
										<td>${list.employeeAddress}</td>
										<td>${list.employeePhone}</td>
										<td>${list.department}</td>
										<td><a href="detail-employee?id=${list.id}"><i class="fa fa-eye"></i> View</a></td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
							<div class="pagination">
								<a href="#">Previous</a>
								<c:forEach items="${numPage}" var="num"> 
									<a href="employee?page=${num}">${num}</a>
								</c:forEach> 
								<a href="#">Next</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		
	</script>
</body>
</body>
</html>