<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Add car</title>
	<link href="<c:url value='template/style.css'/>" rel="stylesheet" type="text/css"/>
	<meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<c:url value='template/vendor/bootstrap/css/bootstrap.min.css'/>">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<c:url value='template/vendor/font-awesome/css/font-awesome.min.css'/>">
    <link rel="stylesheet" href="<c:url value='template/vendor/owl.carousel/assets/owl.carousel.css'/>">
    <link rel="stylesheet" href="<c:url value='template/vendor/owl.carousel/assets/owl.theme.default.css'/>">
    <style type="text/css">
    	body{
    		background-color: #f1f1f1;
    	}
    	.emp-mn{
		 	float: left;
		 	margin-top: 10px;
		 }
		 .main-form tr td{
		 	border: none;
		 }
		 .require{
		 	color: red;
		 	font-weight: bold;
		 }
    </style>
</head>
<body>
<!-- title -->
<div class="col-md-12">
	<div class="row menu">
		<div class="col-sm-8">
			<p class="cms-title"><i class="fa fa-car"></i> Car</p>
		</div>
		<div class="col-sm-2">
			<a href="#">Welcome kasjd</a>
		</div>
		<div class="col-sm-2">
			<a href="#"><i class="fa fa-sign-out"></i>Logout</a>
		</div>
	</div>
</div>

<!-- thanh ngang -->
<div class="col-md-12">
	<hr>	
</div>

<div class="col-md-12 contents">
	<div class="row">

		<!-- bên trái -->
		<div class="col-md-3 content-left">
				<a href="#"><i class="fa fa-dashboard icon-left"></i> Car park
					Manager</a>
				<hr>
				<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-car"></i> Car Manager</a>
					<div class="dropdown-content">
						<a href="car"><i class="fa fa-list"></i> Car List</a> 
						<a href="add-car"><i class="fa fa-plus"></i> Add car</a>
					</div>
				</div>

				<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-map-marker"></i> Booking office
						manager</a>
					<div class="dropdown-content">
						<a href="booking"><i class="fa fa-list"></i> Booking office list</a> <a
							href="add-booking"><i class="fa fa-plus"></i> Add Booking office</a>
					</div>
				</div>

				<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-map-marker"></i> Parking lot manager</a>
					<div class="dropdown-content">
						<a href="parkinglot"><i class="fa fa-list"></i> Parking lot list</a> 
						<a href="add-parkinglot"><i class="fa fa-plus"></i> Add parking lot</a>
					</div>
				</div>
			</div>

		<!-- bên phải -->
		<div class="col-md-9 content-right">
			<h2 class="editTitle">Add Car</h2>
			<hr>
			<div class="col-md-9 element">
					<form:form id="myform" action="update-car" method="post" modelAttribute="car">
						  <table class="table main-form">
						  	<tr>
						  		<td><label class="lb">License plate <span class="require">(*)</span></label></td>
						  		<td><form:input type="text" name="license" class="form-control" placeholder="Enter License plate" path="licensePlate" /></td>
						  	</tr>


						  	<tr>
						  		<td><label class="lb">Car type <span class="require">(*)</span></label></td>
						  		<td><form:input type="text" name="cartype" class="form-control" placeholder="Enter Car type" path="carType" /></td>
						  	</tr>

						  	<tr>
						  		<td><label class="lb">Car color <span class="require">(*)</span></label></td>
						  		<td><form:input type="text" name="carcolor" class="form-control" placeholder="Enter Car color" path="carColor" /></td>
						  	</tr>

						  	<tr>
						  		<td><label class="lb">Company <span class="require">(*)</span></label></td>
						  		<td>
						  			<form:select class="form-control" name="company" path="company">
						  				<option value="phuong trang">Phương trang</option>
						  				<option value="dong anh">Đông anh</option>
						  			</form:select>
						  		</td>
						  	</tr>


						  	<tr>
						  		<td><label class="lb">Parking lot <span class="require">(*)</span></label></td>
						  		<td>
						  			<select class="form-control" name="parking">
						  			<c:forEach items="${listparking}" var="list">
						  				<option value="${list.id}">${list.parkName}</option>
						  			</c:forEach>
						  			</select>
						  		</td>
						  	</tr>


						  	<tr>
						  		<td colspan="2">
						  			<button type="button" class="btn-warning" onclick="reset()"><i class="fa fa-refresh"></i> Reset</button>
						  			<button class="btn-success"><i class="fa fa-plus"></i> Update</button>
						  		</td>
						  	</tr>
						  </table>
					</form:form>
				</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
</script>
</body>
</body>
</html>