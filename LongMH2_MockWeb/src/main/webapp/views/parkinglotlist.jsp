<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Parking Lot List</title>
<link href="<c:url value='template/style.css'/>" rel="stylesheet"
	type="text/css" />
<meta name="robots" content="all,follow">
<!-- Bootstrap CSS-->
<link rel="stylesheet"
	href="<c:url value='template/vendor/bootstrap/css/bootstrap.min.css'/>">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="<c:url value='template/vendor/font-awesome/css/font-awesome.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='template/vendor/owl.carousel/assets/owl.carousel.css'/>">
<link rel="stylesheet"
	href="<c:url value='template/vendor/owl.carousel/assets/owl.theme.default.css'/>">
<style type="text/css">
body {
	background-color: #f1f1f1;
}

.emp-mn {
	float: left;
	margin-top: 10px;
}

.menu-table {
	float: right;
}

.menu-table button {
	margin-left: 10px;
}

.menu-table input {
	border: 0.5px solid #dee3e0;
}

.tables {
	margin-top: 20px;
}

.pagination {
	display: inline-block;
}

.pagination a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
}
.div-search{
 	background-color: #f1f1f1;
 	width: 30px;
 	border: 0.5px solid #dee3e0;
 }
</style>
</head>
<body>
	<!-- title -->
	<div class="col-md-12">
		<div class="row menu">
			<div class="col-sm-8">
				<p class="cms-title">
					<i class="fa fa-shopping-cart"></i> Booking Office
				</p>
			</div>
			<div class="col-sm-2">
				<a href="#">Welcome kasjd</a>
			</div>
			<div class="col-sm-2">
				<a href="#"><i class="fa fa-sign-out"></i>Logout</a>
			</div>
		</div>
	</div>

	<!-- thanh ngang -->
	<div class="col-md-12">
		<hr>
	</div>

	<div class="col-md-12 contents">
		<div class="row">

			<!-- bên trái -->
			<div class="col-md-3 content-left">
				<a href="#"><i class="fa fa-dashboard icon-left"></i> Car Park
					Manager</a>
				<hr>
				<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-car"></i> Car Manager</a>
					<div class="dropdown-content">
						<a href="car"><i class="fa fa-list"></i> Car List</a> 
						<a href="add-car"><i class="fa fa-plus"></i> Add Car</a>
					</div>
				</div>
				<hr>
				<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-map-marker"></i> Booking Office
						Manager</a>
					<div class="dropdown-content">
						<a href="booking"><i class="fa fa-list"></i> Booking Office List</a> <a
							href="add-booking"><i class="fa fa-plus"></i> Add Booking Office</a>
					</div>
				</div>
				<hr>
				<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-map-marker"></i> Parking Lot Manager</a>
					<div class="dropdown-content">
						<a href="parkinglot"><i class="fa fa-list"></i> Parking Lot List</a> 
						<a href="add-parkinglot"><i class="fa fa-plus"></i> Add Parking Lot</a>
					</div>
				</div>
			</div>
			
			<!-- bên phải -->
			<div class="col-md-9 content-right">
				<h2 class="editTitle">Parking Lot List</h2>
				<hr>
				<div class="col-md-12">
					<div class="row">
						<div class="col-sm-4"></div>
						<div class="col-sm-8">
						<form action="parkinglot", method="get" id="search">
							<div class="row menu-table">
								<div class="div-search">
									<p>
										<i class="fa fa-search"></i>
									</p>
								</div>
								<input type="text" name="search"
									placeholder="parking lot list search" class="">
								<button style="" class="btn-filter">
									<i class="fa fa-filter"></i>Filter By
								</button>
								<select>
									<option>Place</option>
								</select>
								<button class="btn-primary">Search</button>
							</div>
							</form>
						</div>
						<div class="col-sm-12 tables">
							<table class="table table-hover">
								<thead>
									<tr>
										<th scope="col">Id</th>
										<th scope="col">parking lost</th>
										<th scope="col">place</th>
										<th scope="col">Area</th>
										<th scope="col">Price</th>
										<th scope="col">Status</th>
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${listParking}" var="list">
									<tr>
										<th scope="row">${list.id}</th>
										<td>${list.parkName}</td>
										<td>${list.parkPlace}</td>
										<td>${list.parkArea}</td>
										<td>${list.parkPrice}</td>
										<td>Blank</td>
										<td><a href="update-parkinglot?id=${list.id}"><i class="fa fa-edit"></i> Edit</a> <a
											href="delete-parkinglot?id=${list.id}"><i class="fa fa-trash"></i> Delete</a></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
							<div class="pagination">
								<a href="#">Previous</a>
								<c:forEach items="${numPage}" var="num"> 
									<a href="parkinglot?page=${num}">${num}</a>
								</c:forEach> 
								<a href="#">Next</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<script type="text/javascript">
		
	</script>
</body>
</body>
</html>