<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>ticket list</title>
<link href="<c:url value='template/style.css'/>" rel="stylesheet"
	type="text/css" />
<meta name="robots" content="all,follow">
<!-- Bootstrap CSS-->
<link rel="stylesheet"
	href="<c:url value='template/vendor/bootstrap/css/bootstrap.min.css'/>">
<!-- Font Awesome CSS-->
<link rel="stylesheet"
	href="<c:url value='template/vendor/font-awesome/css/font-awesome.min.css'/>">
<link rel="stylesheet"
	href="<c:url value='template/vendor/owl.carousel/assets/owl.carousel.css'/>">
<link rel="stylesheet"
	href="<c:url value='template/vendor/owl.carousel/assets/owl.theme.default.css'/>">
<style type="text/css">
body {
	background-color: #f1f1f1;
}

.emp-mn {
	float: left;
	margin-top: 10px;
}

.menu-table-trip {
	float: right;
}

.menu-table-trip button, select, input {
	margin-right: 1rem;
}

.menu-table input {
	border: 0.5px solid #dee3e0;
}

.tables {
	margin-top: 20px;
}

.pagination {
	display: inline-block;
}

.pagination a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
}
.div-search{
 	background-color: #f1f1f1;
 	width: 30px;
 	border: 0.5px solid #dee3e0;
 }
</style>
</head>
<body>
	<!-- title -->
	<div class="col-md-12">
		<div class="row menu">
			<div class="col-sm-8">
				<p class="cms-title">
					<i class="fa fa-ticket"></i> Ticket
				</p>
			</div>
			<div class="col-sm-2">
				<a href="#">Welcome kasjd</a>
			</div>
			<div class="col-sm-2">
				<a href="#"><i class="fa fa-sign-out"></i>Logout</a>
			</div>
		</div>
	</div>

	<!-- thanh ngang -->
	<div class="col-md-12">
		<hr>
	</div>

	<div class="col-md-12 contents">
		<div class="row">

			<!-- bên trái -->
			<div class="col-md-3 content-left">
				<a href="#"><i class="fa fa-plane icon-left"></i> Trip Manager</a>
				<hr>
				<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-car"></i> Trip Manager</a>
					<div class="dropdown-content">
						<a href="trip"><i class="fa fa-list"></i> Trip List</a> 
						<a href="add-trip"><i class="fa fa-plus"></i> Add Trip</a>
					</div>
				</div>
				<hr>
				<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-ticket"></i> Ticket manager</a>
					<div class="dropdown-content">
						<a href="ticket"><i class="fa fa-list"></i> Ticket list</a> 
						<a href="add-ticket"><i class="fa fa-plus"></i> Add Ticket</a>
					</div>
				</div>

			</div>

			<!-- bên phải -->
			<div class="col-md-9 content-right">
				<h2 class="editTitle">Ticket List</h2>
				<hr>
				<div class="col-md-12">
					<div class="row">
						<div class="col-sm-4"></div>
						<div class="col-sm-8">
						<form action="ticket" method="get" id="search">
							<div class="row menu-table-trip">
								<div class="div-search">
									<p>
										<i class="fa fa-search"></i>
									</p>
								</div>
								<input type="text" name="search" placeholder="Users search"
									class="">
								<button class="btn-primary">Search</button>
								<select>
									<option>02</option>
								</select> <select>
									<option>02</option>
								</select> <select>
									<option>2018</option>
								</select>
							</div>
						</form>
						</div>
						<div class="col-sm-12 tables">
							<table class="table table-hover">
								<thead>
									<tr>
										<th scope="col">No</th>
										<th scope="col">Trip</th>
										<th scope="col">Customer</th>
										<th scope="col">License plate</th>
										<th scope="col">Booking Time</th>
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${listTicket}" var="list">
									<tr>
										<th scope="row">${list.id}</th>
										<td>${list.trip.destination}</td>
										<td>${list.customerName}</td>
										<td>${list.car.licensePlate}</td>
										<td>${list.bookingTime}</td>
										<td><a onclick="deleteticket(${list.id})"><i class="fa fa-trash"></i> Delete</a></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
							<div class="pagination">
								<a href="#">Previous</a>
								<c:forEach items="${numPage}" var="num"> 
									<a href="ticket?page=${num}">${num}</a>
								</c:forEach> 
								<a href="#">Next</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<script type="text/javascript">
		function deleteticket(id){
			var r = confirm("are you sure you want to delete this item ?");
			if(r == true){
				window.location.href = "delete-ticket?id="+id;
			}
		}
	</script>
</body>
</body>
</html>