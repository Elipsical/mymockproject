<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Add trip</title>
	<link href="<c:url value='template/style.css'/>" rel="stylesheet" type="text/css"/>
	<meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<c:url value='template/vendor/bootstrap/css/bootstrap.min.css'/>">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<c:url value='template/vendor/font-awesome/css/font-awesome.min.css'/>">
    <link rel="stylesheet" href="<c:url value='template/vendor/owl.carousel/assets/owl.carousel.css'/>">
    <link rel="stylesheet" href="<c:url value='template/vendor/owl.carousel/assets/owl.theme.default.css'/>">
    <style type="text/css">
    	body{
    		background-color: #f1f1f1;
    	}
    	.emp-mn{
		 	float: left;
		 	margin-top: 10px;
		 }
		 .main-form tr td{
		 	border: none;
		 }
		 .require{
		 	color: red;
		 	font-weight: bold;
		 }
    </style>
</head>
<body>
<!-- title -->
<div class="col-md-12">
	<div class="row menu">
		<div class="col-sm-8">
			<p class="cms-title"><i class="fa fa-plane"></i> Trip</p>
		</div>
		<div class="col-sm-2">
			<a href="#">Welcome kasjd</a>
		</div>
		<div class="col-sm-2">
			<a href="#"><i class="fa fa-sign-out"></i>Logout</a>
		</div>
	</div>
</div>

<!-- thanh ngang -->
<div class="col-md-12">
	<hr>	
</div>

<div class="col-md-12 contents">
	<div class="row">

		<!-- bên trái -->
		<div class="col-md-3 content-left">
			<a href="#"><i class="fa fa-plane icon-left"></i> Trip Manager</a>
			<hr>
			<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-car"></i> Trip Manager</a>
					<div class="dropdown-content">
						<a href="trip"><i class="fa fa-list"></i> Trip List</a> 
						<a href="add-trip"><i class="fa fa-plus"></i> Add Trip</a>
					</div>
				</div>

				<div class="dropdown emp-mn">
					<a href=""><i class="fa fa-ticket"></i> Ticket manager</a>
					<div class="dropdown-content">
						<a href="ticket"><i class="fa fa-list"></i> Ticket list</a> 
						<a href="add-ticket"><i class="fa fa-plus"></i> Add Ticket</a>
					</div>
				</div>

		</div>

		<!-- bên phải -->
		<div class="col-md-9 content-right">
			<h2 class="editTitle">Add trip</h2>
			<hr>
			<div class="col-md-9 element">
					<form:form id="myform" action="update-trip" method="post" modelAttribute="trip">
						  <table class="table main-form">
						  	<tr>
						  		<td><label class="lb">Destination <span class="require">(*)</span></label></td>
						  		<td><form:input type="text" name="destination" class="form-control" placeholder="Enter Destination" path="destination"/></td>
						  	</tr>


						  	<tr>
						  		<td><label class="lb">Diparture time <span class="require">(*)</span></label></td>
						  		<td><form:input type="time" name="time" class="form-control" placeholder="Enter Car type" path="departureTime" /></td>
						  	</tr>

						  	<tr>
						  		<td><label class="lb">Driver <span class="require">(*)</span></label></td>
						  		<td><form:input type="text" name="driver" class="form-control" placeholder="Enter Driver" path="driver" /></td>
						  	</tr>

						  	<tr>
						  		<td><label class="lb">Car type <span class="require">(*)</span></label></td>
						  		<td><form:input type="text" name="type" class="form-control" placeholder="Enter Car type" path="carType" /></td>
						  	</tr>


						  	<tr>
						  		<td><label class="lb">Maximum online ticket number <span class="require">(*)</span></label></td>
						  		<td><form:input type="number" name="num" class="form-control" placeholder="Enter Car type" path="bookedTicketNumber" /></td>
						  	</tr>


						  	<tr>
						  		<td><label class="lb">Departure date <span class="require">(*)</span></label></td>
						  		<td><form:input type="date" name="date" class="form-control" placeholder="Enter Car type" path="departureDate" /></td>
						  	</tr>

							<form:input type="hidden" name="id" path="id" />
						  	<tr>
						  		<td colspan="2">
						  			<button type="button" class="btn-warning" onclick="reset()"><i class="fa fa-refresh"></i> Reset</button>
						  			<button class="btn-success"><i class="fa fa-plus"></i> Add</button>
						  		</td>
						  	</tr>
						  </table>
					</form:form>
				</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
</script>
</body>
</body>
</html>