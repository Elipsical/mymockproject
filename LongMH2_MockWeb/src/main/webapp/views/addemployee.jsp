<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Add Employee</title>
	<link href="<c:url value='template/style.css'/>" rel="stylesheet" type="text/css"/>
	<meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<c:url value='template/vendor/bootstrap/css/bootstrap.min.css'/>">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<c:url value='template/vendor/font-awesome/css/font-awesome.min.css'/>">
    <link rel="stylesheet" href="<c:url value='template/vendor/owl.carousel/assets/owl.carousel.css'/>">
    <link rel="stylesheet" href="<c:url value='template/vendor/owl.carousel/assets/owl.theme.default.css'/>">
    <style type="text/css">
    	body{
    		background-color: #f1f1f1;
    	}
    	.emp-mn{
		 	float: left;
		 	margin-top: 10px;
		 }
		 .main-form tr td{
		 	border: none;
		 }
		 .require{
		 	color: red;
		 	font-weight: bold;
		 }
    </style>
</head>
<body>
<!-- title -->
<div class="col-md-12">
	<div class="row menu">
		<div class="col-sm-8">
			<p class="cms-title"><i class="fa fa-users"></i> Employee</p>
		</div>
		<div class="col-sm-2">
			<a href="#">Welcome kasjd</a>
		</div>
		<div class="col-sm-2">
			<a href="#"><i class="fa fa-sign-out"></i>Logout</a>
		</div>
	</div>
</div>

<!-- thanh ngang -->
<div class="col-md-12">
	<hr>	
</div>

<div class="col-md-12 contents">
	<div class="row">

		<!-- bên trái -->
		<div class="col-md-3 content-left">
			<a href="#"><i class="fa fa-dashboard icon-left"></i> Dashboard</a>
			<hr>
			<div class="dropdown emp-mn">
			  <a href=""><i class="fa fa-bar-chart"></i> Employee Manager</a>
			  <div class="dropdown-content">
			    <a href="employee"><i class="fa fa-list"></i> Employee List</a>
			   	<a href="#"><i class="fa fa-plus"></i> Add Employee</a>
			  </div>
			</div>
		</div>

		<!-- bên phải -->
		<div class="col-md-9 content-right">
			<h2 class="editTitle">Add Employee</h2>
			<hr>
				<div class="col-md-9 element">
					<form:form id="myform" action="add-employee" method="post" modelAttribute="employee">
						  <table class="table main-form">
						  	<tr>
						  		<td><label class="lb">Full Name <span class="require">(*)</span></label></td>
						  		<td><form:input type="text" required="required" name="fullname" class="form-control" placeholder="Enter Full Name" path="employeeName" /></td>
						  	</tr>

						  	<tr>
						  		<td><label class="lb">Phone Number <span class="require">(*)</span></label></td>
						  		<td><form:input type="text" required="required" name="phone" class="form-control" placeholder="Enter Phone Number" path="employeePhone" /></td>
						  	</tr>

						  	<tr>
						  		<td><label class="lb">Date of Birth <span class="require">(*)</span></label></td>
						  		<td><form:input type="date" required="required" name="dob" class="form-control" path="employeeBirthDate" /></td>
						  	</tr>

						  	<tr>
						  		<td><label class="lb">Sex <span class="require">(*)</span></label></td>
						  		<td>
						  			<div class="radio">
						  				<form:radiobutton value="1" id="male" path="sex" /><label for="male"> Male</label>
						  				<form:radiobutton value="0" id="female" path="sex" /><label for="female"> Female</label>
						  			</div>
						  		</td>
						  	</tr>


						  	<tr>
						  		<td><label class="lb">Address</label></td>
						  		<td><form:input type="text" name="address" class="form-control" placeholder="Enter Address" path="employeeAddress" /></td>
						  	</tr>

						  	<tr>
						  		<td><label class="lb">Email</label></td>
						  		<td><form:input type="email" name="email" class="form-control" placeholder="Enter Email" path="employeeEmail" /></td>
						  	</tr>

						  	<tr>
						  		<td><label class="lb">Account <span class="require">(*)</span></label></td>
						  		<td><form:input type="text" required="required" name="account" class="form-control" placeholder="Enter Account" path="account"/></td>
						  	</tr>


						  	<tr>
						  		<td><label class="lb">Password <span class="require">(*)</span></label></td>
						  		<td><form:input type="password" required="required" name="password" class="form-control" placeholder="Enter Password" path="password" /></td>
						  	</tr>


						  	<tr>
						  		<td><label class="lb">Department <span class="require">(*)</span></label></td>
						  		<td>
						  			<form:select class="form-control" path="department">
						  				<option value="employee">Employee</option>
						  				<option value="employee">Service</option>
						  				<option value="employee">Parking</option>
						  			</form:select>
						  		</td>
						  	</tr>

						  	<tr>
						  		<td colspan="2">
						  			<button type="button" class="btn-primary" onclick="prePage()"><i class="fa fa-arrow-left"></i> Back</button>
						  			<button type="button" class="btn-warning" onclick="reset()"><i class="fa fa-refresh"></i> Reset</button>
						  			<button class="btn-success"><i class="fa fa-plus"></i> Add</button>
						  		</td>
						  	</tr>
						  </table>
					</form:form>
				</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function prePage(){
		var oldURL = document.referrer;
		window.location.replace(oldURL);
	}

	function reset(){
		document.getElementById('myform').reset();
	}
</script>
</body>
</body>
</html>